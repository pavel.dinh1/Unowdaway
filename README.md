# Unowdaway - Silent Anxiety

Puzzle based game inspired by Amnesia Series, where your goal is to get away from Anxiety and free yourself from the house that has a big history of impacting people with its maze structure.

* While the player is progressing we aim for the Anxiety to let the player struggle in the puzzle game, while making it obvious of the steps
 that will make him feel stupid.

* The next part is to make this game very atmospheric and nice to look to the eyes. The theme itself is the environment
changing while the player progresses. We want it to be as unpredictable as possible.

* Game should be distracting from the theme and the puzzle itself. Because where would be Anxiety without struggle in solving the puzzle.

* There is a chance that the player will be very tired of solving that, so we will provide a simple hint to make the players think in a more different way.

# Gameplay

* The game is simple, you will get the script with description on the puzzle, the thing is the description is the puzzle itself.

* You first have to understand what does the description mean, then figure out how to combine the found things and use it wisely.

* If you struggle with understanding the metaphores of the script, you can use the hint.

* Remember the main goal is to get out of the instance to another till you find the right door to escape from the house.